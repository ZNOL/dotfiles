#!/bin/bash
#
# =============================================================
# Network set up before install
#
# ip link
# 
# or
#
# iwctl
# device list
# station wlan0 scan 
# station wlan0 get-networks
# station wlan0 connect <WIFI_NAME>
#
# =============================================================
# Partion setup
#
# fdisk -l
#
# EFI part
# root part
#
# mkfs.fat -F32 /dev/<EFI>
# mkfs.ext4 /dev/<ROOT>
#
# mount /dev/<ROOT> /mnt
#
# =============================================================
# System install
# 
#
# pacstrap -i /mnt base linux linux-firmware sudo vim
#
# (if get errors use: pacman -Sy archlinux-keyring && pacman-key --refresh-keys
#
# =============================================================
# 
# Gen fstab 
#	
# genfstab -U -p /mnt >> /mnt/etc/fstab
#
# =============================================================
# 
# arch-chroot /mnt /bin/bash
#
# ! Time zone
#
# ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
# hwclock --systohc  --utc
# 
# ! Localization
#
# vim /etc/locale.gen
#
# locale-gen
#
# echo "LANG=en_US.UTF-8" > /etc/locale.conf
#
# ! Hostname
#
# echo arch > /etc/hostname
#
# vim /etc/hosts
# 127.0.0.1 localhost
# ::1       localhost
# 127.0.1.1 arch
#
# passwd
#
# =============================================================

# Reboot to root

# ! Basic

pacman -S base-devel linux-headers bluez bluez-utils alsa-utils openssh tlp virt-manager cpupower
systemctl enable bluetooth
systemctl enable sshd
systemctl enable tlp 
systemctl enable libvirtd
systemctl enable fstrim.timer   # improves ssd performance

# ! Network

pacman -S networkmanager network-manager-applet
systemctl enable NetworkManager

# ! Grub

pacman -S grub efibootmgr
mount --mkdir /dev/nvme0n1p1 /boot/efi
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

# Audio

pacman -S wireplumber pipewire pipewire-alsa pipewire-pulse pipewire-jack

# User account

myuser=znol

useradd -m -g users -G wheel -s /bin/bash $myuser 
echo "$myuser:123" | chpasswd
usermod -aG libvirt $myuser
usermod -aG network $myuser
usermod -aG audio $myuser
usermod -aG video $myuser

echo "%wheel ALL=(ALL) ALL" >> /etc/sudoes.d/ermanno

# EDITOR=vim visudo
#
# uncomment
#
# %wheel ALL=(ALL) ALL

