vim.opt.mouse = "a"

vim.opt.backup = false                          -- creates a backup file
vim.opt.swapfile = false                        -- creates a swapfile
vim.opt.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.fileencoding = "utf-8"                  -- the encoding written to a file

vim.opt.showmode = false                        -- we don't need to see things like -- INSERT -- anymore

vim.opt.termguicolors = true                    -- set term gui colors (most terminals support this)
vim.opt.cmdheight = 1                           -- more space in the neovim command line for displaying messages
vim.opt.ruler = false

vim.opt.number = true                           -- set numbered lines
vim.opt.numberwidth = 4                         -- set number column width to 2 {default 4}
vim.opt.relativenumber = true

vim.opt.hlsearch = true                         -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.incsearch = true                        -- incremental search

vim.opt.smarttab = true
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4                          -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2                             -- insert 2 spaces for a tab
vim.opt.expandtab = true                        -- convert tabs to spaces
vim.opt.cursorline = true                       -- highlight the current line

vim.opt.smartcase = true                        -- smart case
vim.opt.autoindent = true
vim.opt.smartindent = true                      -- make indenting smarter again

vim.opt.scrolloff = 8                           -- Minimum lines to keep above and below 
vim.opt.sidescrolloff = 8                       --          cursos when scrolling

vim.opt.foldenable = false
vim.opt.foldmethod = "manual"
vim.opt.foldnestmax = 1
vim.opt.foldlevel = 2

vim.opt.autochdir = true
-- vim.opt.keymap = "russian-jcukenwin"

vim.opt.clipboard = "unnamedplus"               -- allows neovim to access the system clipboard
vim.opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
vim.opt.conceallevel = 0                        -- so that `` is visible in markdown files
vim.opt.pumheight = 10                          -- pop up menu height
vim.opt.showtabline = 0                         -- always show tabs
vim.opt.splitbelow = true                       -- force all horizontal splits to go below current window
vim.opt.splitright = true                       -- force all vertical splits to go to the right of current window
vim.opt.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = true                         -- enable persistent undo
vim.opt.updatetime = 300                        -- faster completion (4000ms default)
vim.opt.laststatus = 3
vim.opt.showcmd = false
vim.opt.signcolumn = "yes"                      -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap = false                            -- display lines as one long line

vim.opt.fillchars.eob=" "
vim.opt.shortmess:append "c"
vim.opt.whichwrap:append("<,>,[,],h,l")
vim.opt.iskeyword:append("-")
