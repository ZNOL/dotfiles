# Display manager

sudo pacman -Syy
sudo pacman -S gdm
sudo systemctl enable gdm

# Aur manager

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd ..
sudo rm -R paru
paru -Syy

# Basic application

sudo pacman -S firefox alacritty unzip zsh telegram-desktop flameshot brightnessctl neovim bat neofetch tree valgrind wget python-pip mesa exa feh

chsh -s /bin/zsh

# Hyprland install

# Manual
# git clone --recursive https://github.com/hyprwm/Hyprland
# cd Hyprland
# sudo make install
#
# ... TODO
# 
# OR

paru -S hyprland-git
paru -S hyprpaper
paru -S waybar-hyprland-git
paru -S wofi
paru -S wlogout-git
paru -S dunst

# Screen charing on hyprland

sudo pacman -S xdg-desktop-portal-wlr
# check: pacman -Q | grep xdg-desktop-portal-          # make sure you don't have any other xdg-desktop
sudo pacman -S grim slurp

# Add to end of hyprland.conf
# exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
#

# FONT
sudo mkdir /usr/share/fonts/nerd-fonts
cd /usr/share/fonts/nerd-fonts
sudo curl -LJO https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Hack.zip
sudo unzip Hack.zip
sudo rm Hack.zip
fc-cache -vf


