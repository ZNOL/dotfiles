/* High-priority */==============================================================
PulseAudio
--pavucontrol

bluez
--bluetoothctl

tlp

NetworkManager
--nmcli/nmtui

cpupower
--sudo cpupower frequency-set -g powersave

light

xclip
=================================================================================

/* Hardvare video acceleration */================================================
libva-mesa-driver + mesa-vdpau
libva-utils | vainfo
vdpauinfo
=================================================================================

/* AMD  */======================================================================
amd-ucod
--yay -S linux-amd linux-amd-headers
=================================================================================

/* System Apps */================================================================
nnn

grub
--sudo grub-mkconfig -o /boot/grub/grub.cfg
--yay -S update-grub (?)

dwmbar #TODO change to slstatus

xrandr
--xrand --listmonitors
--xrandr --output HDMI-A-0 --auto --left-of eDP --output eDP --off

yay/paru
--pamac
--fakeroot (makepkg -si)
--yay -Yc, --sudo pacman -R $(pacman -Qtdq)
=================================================================================

/* Common Apps */================================================================
unzip

feh

Onlyoffice

ttf-font-awesome
--fc-cache -f -v

neofetch

firefox   about::config
--layers.acceleration.force-enabled / --media.ffmpeg.vaapi.enabled
-- gfx.webrender.all  
-- https://wiki.archlinux.org/title/Firefox_(%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9)#%D0%90%D0%BF%D0%BF%D0%B0%D1%80%D0%B0%D1%82%D0%BD%D0%BE%D0%B5_%D1%83%D1%81%D0%BA%D0%BE%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE 

flameshot

piperwire wireplumber
waybar-hyprland-git

playerctl
waybar-hyprland-git

hyprpaper-git

wofi -----------------------
or simple paru -S wofi

-dependencies
libwayland-dev
libgtk-3-dev
pkg-config
meson

-building
hg clone https://hg.sr.ht/~scoopta/wofi
cd wofi
meson build
ninja -C build

----------------------------
=================================================================================

/* Trash */======================================================================
Thermald  #DELETED
nbfc  #DELETED
fancontrol  #NOT WORKED
acpid  #STOPED
--acpi_listen
--/etc/acpi/handler.sh 
=================================================================================
