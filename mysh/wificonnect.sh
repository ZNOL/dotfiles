#!/bin/sh

bssid=$(nmcli -f BSSID,SSID,SIGNAL,RATE,BARS,SECURITY dev wifi list | sed -n '1!p' | dmenu -p "WIFI: " -l 20 | cut -d' ' -f1)
pass=$(echo "" | dmenu -p "Password: ")
echo "nmcli device wifi connect $bassid password $pass"
nmcli device wifi connect $bssid password $pass

