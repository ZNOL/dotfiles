#!/bin/bash

# xrandr changer

file="/home/znol/dotfiles/xrandr/current_state"

Laptop="eDP"
Monitor="HDMI-A-0"
state=$(cat $file)

if [[ "$state" == "1"  ]]; then
    xrandr --output $Monitor --off --right-of $Laptop --set "TearFree" on --output $Laptop --auto --set "TearFree" on
    echo 2 > $file    
elif [[ "$state" == "2" ]]; then
    xrandr --output $Monitor --auto --right-of $Laptop --set "TearFree" on --output $Laptop --auto --set "TearFree" on
    echo 3 > $file
elif [[ "$state" == "3" ]]; then
    xrandr --output $Monitor --auto --right-of $Laptop --set "TearFree" on --output $Laptop --off --set "TearFree" on
    echo 1 > $file
fi

echo $state
